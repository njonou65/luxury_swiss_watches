import os
from flask import Flask, render_template, request, url_for, redirect , g
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail, Message
from mailjet_rest import Client
from datetime import datetime

from sqlalchemy.sql import func
from sqlalchemy import or_

nbreBuys = 1000

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] =\
        'sqlite:///' + os.path.join(basedir, './instance/products.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config["PRODUCT_PER_PAGE"] = 24
app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'njonougaby45@gmail.com'
app.config['MAIL_PASSWORD'] = 'njonou45'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True

db = SQLAlchemy(app)
mail = Mail(app)

class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)    
    sku = db.Column(db.String(255))
    image1 = db.Column(db.Text , nullable=False)
    image2 = db.Column(db.Text , nullable=False)
    name = db.Column(db.Text, nullable=False)
    price = db.Column(db.Float , nullable=True)
    brand = db.Column(db.String(255))
    category = db.Column(db.Text)
    tag = db.Column(db.Text)
    description = db.Column(db.Text)
    plus_information = db.Column(db.Text)
    collection = db.Column(db.String(255))
    family = db.Column(db.String(255))
    created_at = db.Column(db.DateTime(timezone=True),
                           server_default=func.now())
    def __init__(self,sku,image1,image2,name,price,brand,category,tag,description,plus_information,collection,family): 
        self.sku = sku
        self.image1 = image1
        self.image2 = image2
        self.name = name
        self.price = price
        self.brand = brand
        self.category = category
        self.tag = tag
        self.description = description
        self.plus_information = plus_information
        self.collection = collection
        self.family =   family   
class Countries(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)    
    name = db.Column(db.Text , nullable=False)
    def __init__(self,name): 
        self.image = name

class TypeProduct(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)    
    image = db.Column(db.Text , nullable=False)
    name =  db.Column(db.String(255), nullable=False)
    title = db.Column(db.String(255) , nullable=False)
    collection = db.Column(db.String(255))
    start = db.Column(db.Text)
    end = db.Column(db.Text)
    def __init__(self,name,image,title,start,end , collection): 
        self.image = image
        self.name = name
        self.title = title
        self.start = start
        self.end = end
        self.collection = collection

@app.template_filter()
def numberFormat(value):
    return "$" + format(float(value), ',.2f')

@app.route("/")
def home_page():
    categ = [
           {
            'nom' : 'venus' , 
            'image' : 'watches/venus3.jpg',
            'family' : 'watches',
            'scale' : 1
        },
        {
            'nom' : 'rolex' , 
            'image' : 'img/home/rolex.png',
            'family' : 'watches',
            'scale' : 1
        },
         {
            'nom' : 'audemars-piguet' , 
            'image' : 'img/home/audemars.png',
            'family' : 'watches',
            'scale' : 1
        },
        {
            'nom' : 'patek-philippe' , 
            'image' : 'img/home/patek.png',
            'family' : 'watches',
            'scale' : 1
        },
        {
            'nom' : 'cartier' , 
            'image' : 'img/home/cartier.png',
            'family' : 'watches',
            'scale' : 1
        },
        {
            'nom' : 'hublot' , 
            'image' : 'img/home/hublot.png',
            'family' : 'watches',
            'scale' : 1
        },
           {
            'nom' : 'richard-mille' , 
            'image' : 'img/home/richard-mille.png',
            'family' : 'watches',
            'scale' : 1
        }
           ,
           {
            'nom' : 'omega' , 
            'image' : 'img/home/omega.png',
            'family' : 'watches',
            'scale' : 1
        }
           
        ]
    
    return render_template('index.html' , brands = TypeProduct.query.all() , features = Product.query.limit(8).all() , homeinfo = categ)
 
@app.route("/product/<string:productName>")
def product_page(productName):
    title_page = 'Swiss Luxury watches'    
    g.productName = productName
    return render_template('prod.html' ,  product = Product.query.filter_by(name=  productName).first() , title_page = title_page)

@app.route("/search")
def searchRoute():
    page = request.args.get('page', 1, type=int)
    search = request.args.get('s')
    #pagination = Product.query.order_by(Product.name).paginate(page=page, 
     #                               per_page=app.config["PRODUCT_PER_PAGE"], error_out=False)
    search = str(search).lower()
    print(search)
    
    pagination  = Product.query.filter(or_(
        func.lower(Product.collection).like(f"%{str(search)}%"),
        func.lower(Product.family).like(f"%{str(search)}%"),
        func.lower(Product.category).like(f"%{str(search)}%"),
        func.lower(Product.brand).like(f"%{str(search)}%"),
        func.lower(Product.tag).like(f"%{str(search)}%"),
        func.lower(Product.name).like(f"%{str(search)}%"),
        func.lower(Product.description).like(f"%{str(search)}%"),
        func.lower(Product.plus_information).like(f"%{str(search)}%")
        )).paginate(page=page, 
                                        per_page=app.config["PRODUCT_PER_PAGE"], error_out=False)
    
    return render_template('product_category.html'  , 
                               title_page = '' , 
                               products_list = pagination , 
                               cat_list = [], 
                               category = 'YOU SEARCHED FOR ' + str(search).upper()  , 
                               collection = 'SEARCH' , 
                               description = '',
                               descriptionParagraphe ='' 
                               )

@app.route("/filter/<string:search>")
def filterRoute(search):
    page = request.args.get('page', 1, type=int)
    #pagination = Product.query.order_by(Product.name).paginate(page=page, 
     #                               per_page=app.config["PRODUCT_PER_PAGE"], error_out=False)
    search = str(search).lower()
    print(search)
    
    pagination  = Product.query.filter(or_(
        func.lower(Product.collection).like(f"%{str(search)}%"),
        func.lower(Product.family).like(f"%{str(search)}%"),
        func.lower(Product.category).like(f"%{str(search)}%"),
        func.lower(Product.brand).like(f"%{str(search)}%"),
        func.lower(Product.tag).like(f"%{str(search)}%"),
        func.lower(Product.name).like(f"%{str(search)}%"),
        func.lower(Product.description).like(f"%{str(search)}%"),
        func.lower(Product.plus_information).like(f"%{str(search)}%")
        )).paginate(page=page, per_page=app.config["PRODUCT_PER_PAGE"], error_out=False)
    
    return render_template('product_category.html'  , 
                               title_page = '' , 
                               products_list = pagination , 
                               cat_list = [], 
                               category = 'YOUR FILTER FOR ' + str(search).upper()  , 
                               collection = 'FILTER' , 
                               description = '',
                               descriptionParagraphe ='' 
                               )

@app.route("/category/<famillyProduct>/<collectionProduct>/")
def category_page(famillyProduct , collectionProduct):
    title_page = 'Swiss Luxury watches'
    category_accept = ["rolex","audemars-piguet", "richard-mille","patek-philippe",
                       "cartier","hublot" , "breitling" , "panerai" , "omega" 
                       ,"vacheron-constantin" , "zenit" , "iwc" ,  "necklaces","pendants","rings","bracelets" , "briefcases"]
   
    if  collectionProduct.lower() in category_accept :
        page = request.args.get('page', 1, type=int)
        pagination  = Product.query.filter(Product.collection == collectionProduct.lower(),Product.family == famillyProduct.lower()).paginate(page=page, 
                                        per_page=app.config["PRODUCT_PER_PAGE"], error_out=False)
        pagination2  = TypeProduct.query.filter(TypeProduct.collection == collectionProduct.lower()).paginate(page=page, 
                                        per_page= 50, error_out=False)
        
        allcategories = {
            "rolex" : {"p" : 'Find the most complete catalog of Rolex Watches for sale in Luxury Watches Swiss.$pShop new and pre-owned Rolex timepieces for men and women online or in person, free overnight shipping nationwide.','h1' : "Rolex Watches"},
            "audemars-piguet" : {"p" : 'Find the most complete collection of unworn and pre-owned (used) Audemars Piguet for sale in Luxury Watches Swiss.','h1' : "Audemars Piguet Watches"},
            "richard-mille" : {"p" : 'Find the most complete collection of new and used Richard Mille watches for sale in Luxury Watches Swiss.','h1' : "Richard Mille Watches"},
            "patek-philippe" : {"p" : 'Find the most complete collection of new (unworn) and used (pre-owned) Patek Philippe for sale in Luxury Watches Swiss.','h1' : "Patek Philippe Watches"},
            "cartier" : {"p" : 'Find the best collection of new and used Cartier for sale in Luxury Watches Swiss.','h1' : "Cartier Watches"},
            "hublot" : {"p" : 'Find the most complete collection of new (unworn) and pre-owned (used) Hublot for sale in Luxury Watches Swiss.','h1' : "Hublot Watches"},
            "breitling" : {"p" : 'Find the most complete collection of Breitling WristWatches for sale in Luxury Watches Swiss.','h1' : "Breitling Watches"},
            "panerai" : {"p" : 'Find the most complete collection of Panerai for sale in Luxury Watches Swiss.','h1' : "Panerai Watches"},
            "omega" : {"p" : 'Find the most complete collection of new and pre-owned Omega for sale in Luxury Watches Swiss.','h1' : "Omega Watches"},
            "vacheron-constantin" : {"p" : 'Find the most complete collection of Vacheron Constantin for sale in Luxury Watches Swiss.','h1' : "Vacheron Constantin Watches"},
            "zenit" : {"p" : 'Find the most complete collection of Zenith for sale in Luxury Watches Swiss.','h1' : "Zenith Watches"},
            "iwc" : {"p" : 'Find the most complete collection of IWC watches for sale in Luxury Watches Swiss.','h1' : "IWC Watches"},
            "rings" : {"p" : '','h1' : ""},
            "pendants" : {"p" : '','h1' : ""},
            "necklaces" : {"p" : '','h1' : ""},
            "bracelets" : {"p" : '','h1' : "BRACELETS"},
            "briefcases" : {"p" : 'Find the most complete catalog of Watch Briefcases for sale in Luxury Watches Swiss','h1' : "Watch Storage Briefcases"},
            }
        return render_template('product_category.html'  , 
                               title_page = title_page , 
                               products_list = pagination , 
                               cat_list = pagination2, 
                               category = collectionProduct.replace("-", " ").title() , 
                               collection = famillyProduct.capitalize() , 
                               description = allcategories[collectionProduct.lower()]['h1'],
                               descriptionParagraphe =allcategories[collectionProduct.lower()]['p'] 
                               )
    
    else:
        if collectionProduct.lower() == "omega-swatch" and famillyProduct.lower() == "watches": 
            return render_template('omega-swatch.html')
    return render_template('404.html', title = '404'), 404

@app.route("/category/<famillyProduct>/<collectionProduct>/<brandProduct>")
def brand_page(famillyProduct , collectionProduct , brandProduct):
    title_page = 'Swiss Luxury watches'
    category_accept = ["rolex","audemars-piguet", "richard-mille","patek-philippe",
                       "cartier","hublot" , "breitling" , "panerai" , "omega" 
                       ,"vacheron-constantin" , "zenit" , "iwc" ,  "necklaces","pendants","rings","bracelets"]
    l =  Product.query.filter(func.lower(Product.brand) == func.lower(brandProduct)).with_entities(Product.brand).distinct().count()
   
    #if  collectionProduct.lower() in category_accept and l!=0 :

    if  collectionProduct.lower() in category_accept  :
        page = request.args.get('page', 1, type=int)
        pagination  = Product.query.filter(func.lower(Product.collection) == func.lower(collectionProduct),func.lower(Product.family) == func.lower(famillyProduct) , func.lower(Product.brand) ==  func.lower(brandProduct)).paginate(page=page, 
                                        per_page=app.config["PRODUCT_PER_PAGE"], error_out=False)
        descriptionParagraphe = TypeProduct.query.filter(func.lower(TypeProduct.name) == func.lower(brandProduct)).first() 
        return render_template('product_category.html'  , 
                               title_page = title_page , 
                               products_list = pagination , 
                               cat_list = [], 
                               category = collectionProduct.replace("-", " ").title() , 
                               collection = "Watches" , 
                               brandname = brandProduct , 
                               description = TypeProduct.query.filter(func.lower(TypeProduct.name) == func.lower(brandProduct)).first().title  , 
                               descriptionParagraphe = descriptionParagraphe.start)
    
    return render_template('404.html', title = '404'), 404

@app.route("/buy-watches")
def allwatches():
    title_page = 'Swiss Luxury watches'
    page = request.args.get('page', 1, type=int)
    pagination  = Product.query.filter(Product.family == "watches").paginate(page=page, 
                                        per_page=app.config["PRODUCT_PER_PAGE"], error_out=False)
    p = "Buying a luxury watch online is now easier than ever!$pHave a look at our wide collection of premium products and buy luxury watches at an unbeatable price. Shop for your favorite watch from any device and get our product delivered to your doorsteps with a single tap. You don’t have to worry about the authenticity of our products as we only provide 100% genuine luxury watches to our customers. With us, you never have to second guess or worry about your purchase. We will keep you updated about your order and also provide a money-back guarantee. Here are some reasons why you should buy luxury watches from LuxuryWatchesSwiss."
    return render_template('product_category.html'  , title_page = title_page , products_list = pagination , cat_list = [], category = "" , collection = "LUXURY WATCHES CATALOG" , description = 'LUXURY WATCHES CATALOG' ,descriptionParagraphe = p )
    
@app.route("/about-us")
def about_us():
    return render_template('about.html')
    
@app.route("/cart")
def shop_cart():
    return render_template('shopfirst.html')

@app.route("/checkout")
def checkout():
    return render_template('checkout.html' , countries = Countries.query.order_by(Countries.name.asc()).all())
    
@app.route("/contact")
def contact():
    return render_template('contact.html')

@app.route("/complete",methods = ['POST'])
def complete():
    p = []
    sumProducts = 0
    for x in request.form.getlist('products'):
        x_ = {}
        Y = x.split('####')
        x_['image'] = Y[0]
        x_['name'] = Y[1]
        x_['quantity'] = int(Y[2])
        x_['price'] = float(Y[3])
        sumProducts += x_['price'] * x_['quantity']
        p.append(x_)


    global nbreBuys
    nbreBuys += 1
    # current date and time
    now = datetime.now()

    requete = request.form.to_dict()
    
    
    productsName = requete['firstName'] + requete['lastName']
    productsMail = requete['email']
    productsCountry = requete['Country']
    productsState = requete['state']
    productsCity = requete['city']
    productsStreet = requete['street']
    productsAppartment = requete['Appartment']
    productsZip_code = requete['zip-code']
    productsShipp = requete['shipp']
    productsPayment = requete['payment']
    productsCompagny = requete['compagny']
    productsProducts = request.form.getlist('products')
    productlist= '<ul>'
    for y in productsProducts :
        x = y.split('####')
        productlist +=(f"<li>{x[1]} {x[2]} x {x[3]}</li>")
    productlist+= '</ul>'
        
    
    print(productlist)
    api_key = '9ba01bbb1de7eef7220907cc47b7d6da'
    api_secret = 'b9080ba1238d291814b34544052d2609'
    mailjet = Client(auth=(api_key, api_secret), version='v3.1')
    data = {
    'Messages': [
        {
        "From": {
            "Email": "nzenang82@gmail.com",
            "Name": "Nzenang 82"
        },
        "To": [
            {
            "Email": "swissluxurywatches0@gmail.com",
            "Name": "Swiss Luxury Watches"
            }
        ],
        "Subject": "New customer",
        "TextPart": "Achat",
        "HTMLPart": f"""
        <h1>Customer Information</h1>
        <p><b>Name</b> : {productsName} </p>
        <p>Email : </b>{productsMail} </p>
        <p><b>Localisation :</b>{productsCountry} , {productsState} , {productsCity} , {productsStreet} , {productsAppartment}</p>
        <p><b>Zip-code :</b>{productsZip_code}
        <p><b>Shipping :</b>{productsShipp}</p>
        <p><b>Payment  :</b>{productsPayment}</p>
        <p><b>Compagny :</b>{productsCompagny}</p>
        <h1>Product Information</h1>
        {productlist}
        <p>Total: <b>${sumProducts}<b></p>
        """
        }
    ]
    }
    result = mailjet.send.create(data=data)
    print (result.status_code)
    print (result.json())
    return render_template('complete.html' , products = p , buyNumber = nbreBuys , sumProducts = sumProducts ,
                           totalPrice = sumProducts ,  
                           current_date = now.strftime("%d/%m/%Y, %H:%M:%S"),
                           payment = productsPayment , shipping = requete['shipp'])

@app.route("/filter/")
def prodfilterRoute():
    page = request.args.get('page', 1, type=int)
    #pagination = Product.query.order_by(Product.name).paginate(page=page, 
     #                               per_page=app.config["PRODUCT_PER_PAGE"], error_out=False)
    colorFilter = str(request.args.get('color')).lower()
    sizeFilter = str(request.args.get('size')).lower()
    braceletFilter = str(request.args.get('bracelet')).lower()
    metalFilter = str(request.args.get('metal')).lower()
    minPriceFilter = str(request.args.get('minPrice'))
    maxPriceFilter = str(request.args.get('maxPrice'))

    
    pagination  = Product.query.filter(
    or_(func.lower(Product.collection).like(f"%{str(colorFilter)}%"),
        func.lower(Product.family).like(f"%{str(colorFilter)}%"),
        func.lower(Product.category).like(f"%{str(colorFilter)}%"),
        func.lower(Product.brand).like(f"%{str(colorFilter)}%"),
        func.lower(Product.tag).like(f"%{str(colorFilter)}%"),
        func.lower(Product.name).like(f"%{str(colorFilter)}%"),
        func.lower(Product.description).like(f"%{str(colorFilter)}%"),
        func.lower(Product.plus_information).like(f"%{str(colorFilter)}%")
        ),
    or_(func.lower(Product.collection).like(f"%{str(sizeFilter)}%"),
        func.lower(Product.family).like(f"%{str(sizeFilter)}%"),
        func.lower(Product.category).like(f"%{str(sizeFilter)}%"),
        func.lower(Product.brand).like(f"%{str(sizeFilter)}%"),
        func.lower(Product.tag).like(f"%{str(sizeFilter)}%"),
        func.lower(Product.name).like(f"%{str(sizeFilter)}%"),
        func.lower(Product.description).like(f"%{str(sizeFilter)}%"),
        func.lower(Product.plus_information).like(f"%{str(sizeFilter)}%")
        ),
    or_(func.lower(Product.collection).like(f"%{str(braceletFilter)}%"),
        func.lower(Product.family).like(f"%{str(braceletFilter)}%"),
        func.lower(Product.category).like(f"%{str(braceletFilter)}%"),
        func.lower(Product.brand).like(f"%{str(braceletFilter)}%"),
        func.lower(Product.tag).like(f"%{str(braceletFilter)}%"),
        func.lower(Product.name).like(f"%{str(braceletFilter)}%"),
        func.lower(Product.description).like(f"%{str(braceletFilter)}%"),
        func.lower(Product.plus_information).like(f"%{str(braceletFilter)}%")
        ),
    or_(func.lower(Product.collection).like(f"%{str(metalFilter)}%"),
        func.lower(Product.family).like(f"%{str(metalFilter)}%"),
        func.lower(Product.category).like(f"%{str(metalFilter)}%"),
        func.lower(Product.brand).like(f"%{str(metalFilter)}%"),
        func.lower(Product.tag).like(f"%{str(metalFilter)}%"),
        func.lower(Product.name).like(f"%{str(metalFilter)}%"),
        func.lower(Product.description).like(f"%{str(metalFilter)}%"),
        func.lower(Product.plus_information).like(f"%{str(metalFilter)}%")
        ),
        Product.price >= float(minPriceFilter),
        Product.price <= float(maxPriceFilter),

    ).paginate(page=page, per_page=app.config["PRODUCT_PER_PAGE"], error_out=False)
    
    return render_template('product_category.html'  , 
                               title_page = '' , 
                               products_list = pagination , 
                               cat_list = [], 
                               category = 'YOUR FILTER ', 
                               collection = 'FILTER' , 
                               description = '',
                               descriptionParagraphe ='' 
                               )

if __name__ == "__main__":
    app.run()