import os
from turtle import title
from flask import Flask, render_template, request, url_for, redirect
from flask_sqlalchemy import SQLAlchemy

from sqlalchemy.sql import func


basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] =\
        'sqlite:///' + os.path.join(basedir, './instance/products.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config["PRODUCT_PER_PAGE"] = 15
db = SQLAlchemy(app)
class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)    
    sku = db.Column(db.String(255))
    image1 = db.Column(db.Text , nullable=False)
    image2 = db.Column(db.Text , nullable=False)
    name = db.Column(db.Text, nullable=False)
    price = db.Column(db.Float , nullable=True)
    brand = db.Column(db.String(255))
    category = db.Column(db.Text)
    tag = db.Column(db.Text)
    description = db.Column(db.Text)
    plus_information = db.Column(db.Text)
    collection = db.Column(db.String(255))
    family = db.Column(db.String(255))
    created_at = db.Column(db.DateTime(timezone=True),
                           server_default=func.now())
    def __init__(self,sku,image1,image2,name,price,brand,category,tag,description,plus_information,collection,family): 
        self.sku = sku
        self.image1 = image1
        self.image2 = image2
        self.name = name
        self.price = price
        self.brand = brand
        self.category = category
        self.tag = tag
        self.description = description
        self.plus_information = plus_information
        self.collection = collection
        self.family =   family   
           

@app.route("/")
def home_page():
    return render_template('index.html')
 
@app.route("/product/<productName>")
def product_page(productName):
    title_page = 'Swiss Luxury watches'
    
    return render_template('product.html' ,  product = Product.query.filter_by(name= productName).first() , title_page = title_page)

@app.route("/test")
def test():
    page = request.args.get('page', 1, type=int)
    pagination = Product.query.order_by(Product.name).paginate(page=page, 
                                    per_page=app.config["PRODUCT_PER_PAGE"], error_out=False)
    
    return render_template('product.html')

@app.route("/category/rolex/")
def rolex_page():
    title_page = 'Swiss Luxury watches'
    page = request.args.get('page', 1, type=int)
    pagination  = Product.query.order_by(Product.name).paginate(page=page, 
                                    per_page=app.config["PRODUCT_PER_PAGE"], error_out=False)
    return render_template('product_category.html'  , title_page = title_page , products_list = pagination)

