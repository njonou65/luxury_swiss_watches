
let homeSwiper = new Swiper(".mySwiper", {
  loop: 'true',
  effect: 'fade',
  autoplay: {
    delay: 7000,
    disableOnInteraction: false,
  },
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
});



// sticky menu
window.addEventListener("scroll", (event) => {
  let header = document.getElementById("myheader");
  header.classList.toggle("sticky", window.scrollY > 160)
  header.classList.toggle("fade-in", window.scrollY > 160)

});
window.addEventListener('scroll', () => {
  document.documentElement.style.setProperty('--scroll-y', `${window.scrollY}px`);
});
const settings_google = {
  loop: true,

  speed: 700,

  pagination: {
    el: ".swiper-pagination",
    type: "bullets"
  },

  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev"
  }
};

const swiper = new Swiper(".googleSwipper", settings_google);

dollar_format = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
});

modalinfo = function (element) {
  document.getElementById('modal_image').src = "/static/" + element.getAttribute('data-productImage2')
  document.getElementById('modal_name').innerHTML = element.getAttribute('data-productName')
  document.getElementById('modal_price').innerHTML = dollar_format.format(element.getAttribute('data-productPrice'))
  document.getElementById('modal_sku').innerHTML = element.getAttribute('data-productSku')
  document.getElementById('modal_categorie').innerHTML = element.getAttribute('data-productCategory')
  document.getElementById('modal_tags').innerHTML = element.getAttribute('data-productTag')
  console.log(document.getElementById('modal_image').src)
  document.getElementById('whatsapp_discus').href = "https://wa.me/+237657815959?text=I am interested in the following watch : " + element.getAttribute('data-productName')
}


